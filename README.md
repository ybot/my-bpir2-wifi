# firmware and/or binary blops for the Banana Pi R2


# build full new rootfs img
check out `build-new-rootimg.sh`

# Wifi related stuff:
check out `build-wifi-tools.sh` and afterwards build deb package. you'll get the my-bpir2-wifi package that incluldes all wifi tweaks

# resources
based on repo/sources of:

### wifi:
- `https://github.com/BPI-SINOVOIP/BPI-R2-bsp/tree/v1.2.1/vendor/mediatek/connectivity`

### rootfs
- `https://github.com/BPI-SINOVOIP/BPI-files`
- `https://github.com/frank-w/BPI-R2-4.14`
- `https://github.com/frank-w/u-boot.git`

