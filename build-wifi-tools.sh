#!/bin/bash
set -Eeuo pipefail

#download https://github.com/BPI-SINOVOIP/BPI-R2-bsp/tree/v1.2.1/vendor/mediatek/connectivity  to temp-folder
#you can also "trust" the prebuild binaries that come already in tools/binary, which you may as well since all the other firmware blops we kinda need to trust


rm -rf temp
mkdir temp
cd temp
for file in config/WMT_SOC.cfg firmware/nvram/WIFI firmware/ROMv2_lm_patch_1_0_hdr.bin firmware/ROMv2_lm_patch_1_1_hdr.bin firmware/WIFI_RAM_CODE_7623 tools/src/stp_uart_launcher.c tools/src/wmt_ioctl.h tools/src/wmt_loader.c tools/src/wmt_loopback.c; do
	mkdir -p ${file%/*} || /bin/true
	curl -Lo ${file} --fail https://raw.githubusercontent.com/BPI-SINOVOIP/BPI-R2-bsp/v1.2.1/vendor/mediatek/connectivity/${file}
done

for srcfile in tools/src/stp_uart_launcher.c tools/src/wmt_loader.c tools/src/wmt_loopback.c; do
	arm-linux-gnueabihf-gcc ${srcfile} -lpthread -o ${srcfile%.*}
done
